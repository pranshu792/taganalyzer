<!DOCTYPE HTML>
<html>
    <head>
        <title>The Krypto Project</title>
        <meta content="" name="description" />
        <meta content="pranshu jhalani" name="author" />
        <link rel="icon" href="assets/Krypto/favicon.ico" type="image/icon">
        <link href="assets/css/main.css" rel="stylesheet" media="screen">
        <?php
        ob_start();
        ?>
    </head>
    <body>
        <div id="navigationBottom">
                    <div class="horizontalNavigationBar">
                        <ul class="content-navigation">
                            <li>
                                <div>
                                    <a href=""><span>About</span></a>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <a href=""><span>Archive</span></a>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <a href="logout.php"><span>Logout</span></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
    <div id="canvasWrapper">
        <div id="canvas">
            <div id="pageHeaderwrapper">
                <div id="pageHeader">
                    <div id="bannerAreaWrapper">
                        <div id="bannerArea">
                            <div id ="bannerWrapper">
                                <img id="banner" src="assets/Krypto/krypto.png" alt="Krypto Project" title="Krypto Project">
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>